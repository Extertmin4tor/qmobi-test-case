To start
```bash
docker-compose up
```

Request example:

```bash
http://127.0.0.1:8080/convert?convert_from=USD&convert_to=RUB&value=2
```

Tests:

```bash
python -m unittest
```