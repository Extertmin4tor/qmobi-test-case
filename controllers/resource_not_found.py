import json
from typing import Tuple

from controllers.base import BaseController


class ResourceNotFoundController(BaseController):
    def handle(self, params: dict) -> Tuple[bytes, int]:
        """
        Handle params and do some business logic.

        Args:
            params: Параметры GET - запроса.
        """
        return json.dumps(
            {'error': "This resource is not avaible!"}).encode(), 404
