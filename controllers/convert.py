import json
from typing import Tuple
from urllib.error import URLError

from converter import CurrencyConverter
from controllers.base import BaseController


class ConvertController(BaseController):
    ALLOWED_KEYS = ['from_currency', 'to_currency', 'value']

    def __init__(self):
        self._params = {}

    def _validate(self, params: dict) -> dict:
        """
        Валидация входных значений.

        Args:
            params: Параметры

        Raises:
            KeyError: В запросе не хватает параметров
            TypeError: Значение не является числовым типом
        """
        validated_params = {}
        for key in self.ALLOWED_KEYS:
            validated_params[key] = params[key][0]
        if not validated_params['value'].isdigit():
            raise TypeError
        return validated_params

    def handle(self, params: dict) -> Tuple[bytes, int]:
        """
         Handle params and do some business logic.

         Args:
             params: Параметры GET - запроса.
         """
        response_code = None
        try:
            params = self._validate(params)
            result = CurrencyConverter(
                float(params['value']),
                params['from_currency'],
                params['to_currency']).convert()
            response_code = 200
        except KeyError:
            result = {'error': "Wrong params!"}
        except TypeError:
            result = {'error': "Value MUST be a positive number type!"}
        except URLError:
            result = {
                'error': "Converter resource not available, try again later"}
        result = json.dumps(result).encode()

        return result, response_code or 400
