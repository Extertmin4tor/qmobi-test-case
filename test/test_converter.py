import unittest
from unittest.mock import patch, Mock

from converter import CurrencyConverter


@patch('converter.CurrencyConverter._make_request',
       Mock(return_value={'USD_RUB': "30.3"}))
class TestConverter(unittest.TestCase):
    def test_converter(self):
        params = {
            'value': 2,
            'from_currency': "USD",
            'to_currency': "RUB"
        }

        result = CurrencyConverter(**params).convert()

        self.assertEqual(result['result'], 60.6)
        self.assertEqual(result['currency_pair'], f"USD_RUB")
        self.assertEqual(result['requested_value'], 2)
