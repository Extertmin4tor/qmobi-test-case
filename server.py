import time
import logging

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs

from conf import HOST_NAME, PORT_NUMBER
from controllers.convert import ConvertController
from controllers.resource_not_found import ResourceNotFoundController


class RequestHandler(BaseHTTPRequestHandler):
    ROUTES = {
        "/convert": ConvertController,
        "/": ConvertController
    }

    def send_response(self, code, message=None) -> None:
        """
        Add the response header to the headers buffer and log the
        response code.

        Also send two standard headers with the server software
        version and the current date.

        Args:
            code: Response code
            message: Response message
        """
        self.log_request(code)
        self.send_response_only(code)
        self.send_header('Server', 'http.server')
        self.send_header('Date', self.date_time_string())
        self.end_headers()

    def do_GET(self):
        """Response for a GET request."""
        query = urlparse(self.path).query
        path = urlparse(self.path).path
        params = parse_qs(query)
        request_controller = self.ROUTES.get(path, ResourceNotFoundController)
        result, code = request_controller().handle(params)

        self.send_response(code)
        self.send_header('Content-Type', 'application/json')
        self.wfile.write(result)


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """
    Run server.

    Args:
        server_class: Http server class
        handler_class: Handler class
    """
    logging.basicConfig(level=logging.INFO)
    server_address = (HOST_NAME, PORT_NUMBER)
    httpd = server_class(server_address, handler_class)
    logging.info(
        '%s: Server UP - %s:%s' % (time.asctime(), HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
        logging.info('%s: Server DOWN - %s:%s' % (
            time.asctime(), HOST_NAME, PORT_NUMBER))


if __name__ == '__main__':
    run(handler_class=RequestHandler)
