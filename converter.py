import json

from urllib.parse import urlencode
from urllib.request import urlopen

from conf import CONVERTER_API_URL, API_KEY


class CurrencyConverter:
    def __init__(self, value: float, from_currency: str, to_currency: str):
        """
        Args:
            value: Количество валюты from_currency
            from_currency: Тип валюты, которую нужно конвертировать
            to_currency: Тип валюты, в который нужно конвертировать
        """
        self._value = value
        self._currency_pair = f"{from_currency}_{to_currency}"

    def convert(self) -> dict:
        """Convert value to another currency by currency_pair rule."""
        params = self._prepare_params()
        data = self._make_request(params)
        return self._prepare_data(data)

    def _prepare_data(self, data: dict) -> dict:
        """
        Preparing data to return as result.

        Args:
            data: Data from api.
        """
        coefficient = float(data[self._currency_pair])
        return {
            'result': self._value * coefficient,
            'currency_pair': self._currency_pair,
            'requested_value': self._value,
        }

    def _prepare_params(self) -> str:
        """Preparing request params."""
        params = {
            'apiKey': API_KEY,
            'q': self._currency_pair,
            'compact': "ultra",
        }
        return urlencode(params)

    @staticmethod
    def _make_request(params: str) -> dict:
        """
        Make request to remote API.

        Args:
            params: Prepared params.
        Raises:
            URLError: Something wrong with connection
        """
        with urlopen(f'{CONVERTER_API_URL}?{params}') as response:
            return json.loads(response.read().decode())
