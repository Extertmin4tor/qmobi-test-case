import json
import unittest
from unittest.mock import patch
from urllib.error import URLError

from controllers.convert import ConvertController
from controllers.resource_not_found import ResourceNotFoundController


class TestController(unittest.TestCase):
    def test_resouce_not_found(self):
        params = {'a': ['a'], 'b': ['b']}

        result, code = ResourceNotFoundController().handle(params)

        self.assertEqual(result, json.dumps(
            {'error': "This resource is not avaible!"}).encode())
        self.assertEqual(code, 404)

    def test_resouce_not_found_empty(self):
        params = {}

        result, code = ResourceNotFoundController().handle(params)

        self.assertEqual(result, json.dumps(
            {'error': "This resource is not avaible!"}).encode())
        self.assertEqual(code, 404)

    @patch("converter.CurrencyConverter.convert")
    def test_convert(self, currency_converter_convert):
        response = {'result': 'test'}
        currency_converter_convert.return_value = response
        params = {'from_currency': ['a'], 'to_currency': ['b'], 'value': ["1"]}

        result, code = ConvertController().handle(params)

        self.assertEqual(result, json.dumps(response).encode())
        self.assertEqual(code, 200)

    @patch("converter.CurrencyConverter.convert")
    def test_convert_empty(self, currency_converter_convert):
        response = {'result': 'test'}
        currency_converter_convert.return_value = response
        params = {}

        result, code = ConvertController().handle(params)

        self.assertEqual(result, b'{"error": "Wrong params!"}')
        self.assertEqual(code, 400)

    @patch("converter.CurrencyConverter.convert")
    def test_convert_wrong_params(self, currency_converter_convert):
        response = {'result': 'test'}
        currency_converter_convert.return_value = response
        params = {'wrong_param': ['a'], 'to_currency': ['b'], 'value': ["1"]}

        result, code = ConvertController().handle(params)

        self.assertEqual(result, b'{"error": "Wrong params!"}')
        self.assertEqual(code, 400)

    @patch("converter.CurrencyConverter.convert")
    def test_convert_wrong_value_type(self, currency_converter_convert):
        response = {'result': 'test'}
        currency_converter_convert.return_value = response
        params = {'from_currency': ['a'], 'to_currency': ['b'], 'value': ["a"]}

        result, code = ConvertController().handle(params)

        self.assertEqual(result,
                         b'{"error": "Value MUST be a positive number type!"}')
        self.assertEqual(code, 400)

    @patch("converter.CurrencyConverter.convert")
    def test_convert_wrong_connection(self, currency_converter_convert):
        currency_converter_convert.side_effect = URLError("test")
        params = {'from_currency': ['a'], 'to_currency': ['b'], 'value': ["1"]}

        result, code = ConvertController().handle(params)

        self.assertEqual(result,
                         b'{"error": "Converter resource not'
                         b' available, try again later"}')
        self.assertEqual(code, 400)
