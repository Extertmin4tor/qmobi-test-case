from abc import ABC, abstractmethod
from typing import Tuple


class BaseController(ABC):
    @abstractmethod
    def handle(self, params: dict) -> Tuple[bytes, int]:
        """
         Handle params and do some business logic.

         Args:
             params: Параметры GET - запроса.

         """
        pass
